<?php

$config = array(
    //内容
    'content' => array(
        array(
            'field' => 'title',
            'label' => '标题',
            'rules' => 'required'
        ),
        array(
            'field' => 'show_num',
            'label' => '浏览量',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'cid',
            'label' => '所属分类',
            'rules' => 'integer'
        ),
      
        array(
            'field' => 'is_visable',
            'label' => '是否显示',
            'rules' => 'required|integer'
        )
    ),
    //分类
    'category' => array(
        array(
            'field' => 'title',
            'label' => '标题',
            'rules' => 'required'
        ),
    ),
    //登陆
    'login' => array(
        array(
            'field' => 'user',
            'label' => '用户名',
            'rules' => 'required'
        ),
        array(
            'field' => 'pwd',
            'label' => '密码',
            'rules' => 'required'
        ),
        array(
            'field' => 'ver',
            'label' => '验证码',
            'rules' => 'required'
        ),
        
    ),
    //链接
    'link' => array(
        array(
            'field' => 'title',
            'label' => '链接名称',
            'rules' => 'required'
        ),
        array(
            'field' => 'src',
            'label' => '链接地址',
            'rules' => 'required'
        ),
        array(
            'field' => 'flag',
            'label' => '标识',
            'rules' => 'required'
        ),
        
    ),
);









