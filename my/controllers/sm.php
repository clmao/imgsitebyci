<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sm extends CI_Controller {

    public function __construct() {
        parent::__construct();
        define('BLOG.CLMAO.COM', 'BLOG.CLMAO.COM');
        include './config.php';
        $this->load->model('content_model', 'art');
        $this->load->model('category_model', 'category');
        $this->load->model('link_model', 'link');
    }

    //网站首页
    public function index() {
        $number = $this->art->count_content();
        $this->load->library('pagination');
        $config['base_url'] = site_url('sm/index/'); //路径            
        $config['total_rows'] = $number;  //配置记录总条数          
        $config['per_page'] = 6; //配置每页显示的记录数 
        $config['first_tag_open'] = '<div>';
        $config['first_tag_close'] = '</div>';
        $config['uri_segment'] = 3;
        $config['next_link'] = '下一页';
        $config['prev_link'] = '上一页';
        $config['last_link'] = '末页';
        $config['first_link'] = '首页';
        $config['num_links'] = 6;
        $config['full_tag_open'] = '<div  id="pagenavi">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open'] = '<span>';
        $config['cur_tag_close'] = '</span>';
        //配置偏移量在url中的位置  
        $config['cur_page'] = $this->uri->segment(3);
        $tab['table'] = $this->art->list_posts_index($config ['per_page'], $this->uri->segment(3)); //当前页显示的数据 
        $this->pagination->initialize($config);
        $this->load->view('sm_index', $tab);   //调页面  传数据
    }

    //显示具体的分类
    public function category($id = 1) {
        $id = $id + 0;
        $number = $this->art->cid_conent($id);
        $this->load->library('pagination');
        $config['base_url'] = site_url('sm/index/'); //路径            
        $config['total_rows'] = $number;  //配置记录总条数          
        $config['per_page'] = 6; //配置每页显示的记录数 
        $config['first_tag_open'] = '<div>';
        $config['first_tag_close'] = '</div>';
        $config['uri_segment'] = 3;
        $config['next_link'] = '下一页';
        $config['prev_link'] = '上一页';
        $config['last_link'] = '末页';
        $config['first_link'] = '首页';
        $config['num_links'] = 6;
        $config['full_tag_open'] = '<div  id="pagenavi">';
        $config['full_tag_close'] = '</div>';
        $config['cur_tag_open'] = '<span>';
        $config['cur_tag_close'] = '</span>';
        //配置偏移量在url中的位置  
        $config['cur_page'] = $this->uri->segment(3);
        $tab['table'] = $this->art->list_posts_catgory($config ['per_page'], $this->uri->segment(3), $id); //当前页显示的数据 
        $this->pagination->initialize($config);
        $tab['flag'] = 'category';
      //  echo $this->db->last_query();
      //  print_r($tab);die;
        $this->load->view('sm_index', $tab);   //调页面  传数据
    }

    //显示具体的内容页
    public function content($id = 0) {
        $id = $id + 0;
        $data['res'] = $this->art->content_index($id);
        $data['next'] = $this->art->next_content($id);
        $data['prev'] = $this->art->prev_content($id);
        $this->load->view('sm_content', $data);
    }

    public function commHeader() {
        
    }

    public function footer() {
        $this->load->view('sm_footer');
    }

    /**
     * 显示登录
     */
    public function login() {
        $admin = $this->session->userdata('admin');

        if ($admin == "vnuytxfzjhkhj") {
            redirect("/sa/index", 'location');
        }
        $this->load->helper('form');
        $this->load->view('sm_login');
    }

    //验证登录
    public function checkLogin() {
        $this->load->library('form_validation');
        $status = $this->form_validation->run('login');
        if ($status) {
            $verify = $this->session->userdata('verify');
            $ver = $this->input->post('ver');
            $pwd = $this->input->post('pwd');
            if (md5(md5($ver)) == $verify && $pwd == ADMINPWS) {
                $this->session->set_userdata('admin', 'vnuytxfzjhkhj');
                redirect("/sa/index", 'location');
            }
            redirect("/sm/login?flag=e", 'location');
        } else {
            $this->load->helper('form');
            $this->load->view('sm_login');
        }
    }

    /**
     * 生成验证码
     */
    public function verify() {
        Header("Content-type: image/PNG");
        srand((double) microtime() * 1000000);
        $im = imagecreate(62, 20);
        $black = ImageColorAllocate($im, 69, 120, 239);
        $white = ImageColorAllocate($im, 0, 0, 0);
        $gray = ImageColorAllocate($im, 200, 200, 200);
        imagefill($im, 68, 30, $gray);
        while (($authnum = rand() % 100000) < 10000);
        //将五位整数验证码绘入图片
        imagestring($im, 5, 10, 3, $authnum, $white);
        for ($i = 0; $i < 200; $i++) { //加入干扰象素
            $randcolor = ImageColorallocate($im, rand(0, 255), rand(0, 255), rand(0, 255));
            imagesetpixel($im, rand() % 70, rand() % 30, $randcolor);
        }
        ImagePNG($im);
        ImageDestroy($im);
        $authnum = $authnum +( VERRULE);
        $authnum = md5(md5($authnum));
        $this->session->set_userdata('verify', $authnum);
    }

    

      

    

}
