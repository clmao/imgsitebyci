<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 内容管理模型
 */
class Content_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 发表内容
     */
    public function add($data) {
        $this->db->insert('content', $data);
    }

    /**
     * 获得文章总数
     */
    public function count_content() {
        return $this->db->count_all('content');
    }
     /**
     * 获取分类下的文章总数
     */
    public function cid_conent($c_id) {
        return $this->db->count_all("content where c_id=$c_id");
    }
    /**
     * 获得文章列表，分页,后台使用
     */
    public function list_posts($limit, $offset) {
        $this->db->limit($limit, $offset);
        $data = $this->db->select('content.id,content.title,show_num,is_visable,category.id as c_id2, category.title as c_title,time,thumb')->from('content')->join('category', 'content.c_id=category.id')->order_by('content.id', 'desc')->get()->result_array();
        return $data;
    }

    /**
     * 获得文章列表，分页，前台使用
     */
    public function list_posts_index($limit, $offset) {
        $this->db->where('is_visable', 1);
        $this->db->limit($limit, $offset);
        $data = $this->db->select('content.id,content.title,content.c_id,show_num,category.id as c_id2, category.title as c_title,thumb')->from('content')->join('category', 'content.c_id=category.id')->order_by('content.id', 'desc')->get()->result_array();
        return $data;
    }
    /**
     * 获得文章列表，分页，前台分类页面使用
     */
    public function list_posts_catgory($limit, $offset=0,$c_id) {
        $this->db->where('sm_content.is_visable', 1);
        $this->db->where('sm_category.id', $c_id);
        $this->db->limit($limit, 0);
        $data = $this->db->select('content.id,content.title,content.c_id,show_num,category.id as c_id2, category.title as c_title,thumb')->from('content')->join('category', 'content.c_id=category.id')->order_by('content.id', 'desc')->get()->result_array();
        return $data;
    }

    /**
     * 获取分类id
     */
    public function get_cid() {
        $this->db->select('id,title');
        $this->db->from('category');
        return $this->db->get()->result_array();
    }

    /**
     * 删除内容
     */
    public function del_content($id = 0) {
        $this->db->select('big_img');
        $this->db->from('content');
        $this->db->where('id', $id);
        $img = $this->db->get()->result_array();
        $img = $img[0]['big_img'];
        unlink($_SERVER['DOCUMENT_ROOT'] . "/sm/up/" . $img);
        unlink($_SERVER['DOCUMENT_ROOT'] . "/sm/bp/" . $img);
        $this->db->where('id', $id);
        $this->db->delete('content');
    }

    /**
     * 
     * 获取具体的内容
     */
    public function get_content($id) {
        $this->db->select('content');
        $this->db->from('content');
        $this->db->where('id', $id);
        return $this->db->get()->result_array();
    }

    /*
     * 更新内容
     */

    public function update_content($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('content', $data);
        $this->db->set();
    }

    /*
     * 前台内容页
     */

    public function content_index($id) {
        $data = $this->db->select('content.id,content.title,content,category.id as c_id2, category.title as c_title,thumb')->join('category', 'content.c_id=category.id')->get_where('content', array('content.id' => $id))->result_array();
        return $data;
    }
    
    /*
     * 获取上一篇文章的ID
     */
    public function prev_content($id) {
        $this->db->where('id >',$id);
        $this->db->limit(1, 0);
        $data = $this->db->select('id')->get('content')->result_array();
        if (empty($data)) {
            $this->db->limit(1, 0);
            $data = $this->db->select('id')->get('content')->result_array();
            
        }
        return $data;
    }
    /*
     * 获取下一篇文章的ID
     */
    public function next_content($id) {
        $this->db->where('id <',$id);
        $this->db->limit(1, 0);
        $data = $this->db->select('id')->order_by('id', 'desc')->get('content')->result_array();
        //echo $this->db->last_query();die;
        if (empty($data)) {
            $this->db->limit(1, 0);
            $data = $this->db->select('id')->order_by('id', 'desc')->get('content')->result_array();
        }
        return $data;
    }
    /*
     * 获取所有文章的ID
     */
    public function all_content_id() {
        $this->db->where('is_visable',1);
        $data = $this->db->select('id,time,title')->order_by('id', 'desc')->get('content')->result_array();
        return $data;
    }
}
