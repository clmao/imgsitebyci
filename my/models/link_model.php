<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 链接管理模型
 */
class Link_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加链接
     */
    public function add($data) {
        $this->db->insert('link', $data);
    }
    
    /*
     * 查询所有链接
     * flag:标示
     */
    public function get_all_link($flag){
        $this->db->where('flag', $flag);
        $data = $this->db->select('id,title,src,flag')->from('link')->get()->result_array();
        return $data;
    }
    /**
     * 删除链接
     */
    public function del_link($id){
        $this->db->where('id', $id);
        $this->db->delete('link');
    }
  

    
}
