<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 内容管理模型
 */
class Category_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加分类
     */
    public function add($data) {
        $this->db->insert('category', $data);
    }

    /**
     * 获得分类
     */
    public function count_category() {
        return $this->db->count_all('category');
    }

    /**
     * 获得分类列表，分页
     */
    public function list_posts($limit, $offset) {
        $this->db->limit($limit, $offset);
        $data = $this->db->select('id,title')->from('category')->order_by('id', 'asc')->get()->result_array();
        return $data;
    }

    /**
     * 获取分类下的文章总数
     */
    public function cid_conent($c_id) {
        return $this->db->count_all("content where c_id=$c_id");
    }

    /**
     * 更新分类
     */
    public function update_category($id,$title) {
        $this->db->where('id', $id);
        $this->db->update('category', array('title'=>$title));
        $this->db->set();
    }
    
    /**
     * 删除分类
     */
    public function del_category($id=100){
        $this->db->where('id', $id);
        $this->db->delete('category');
    }
    /**
     * 获取所有分类
     */
    public function get_all_category(){
        $data = $this->db->select('id,title')->from('category')->order_by('id', 'asc')->get()->result_array();
        return $data;
    }

}
