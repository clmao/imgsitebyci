<!DOCTYPE HTML>
<html>
    <head>
        <script type="text/javascript">
            function pageNew()
            {
                if (event.keyCode == 37)
                {
                    window.location.href = "<?php echo site_url('sm/content/' . ($prev[0]['id'])) ?>";
                }
                if (event.keyCode == 39)
                {
                    window.location.href = "<?php echo site_url('sm/content/' . ($next[0]['id'])); ?>";
                }
            }
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $res[0]['title']; ?>_<?php echo $res[0]['c_title']; ?>_<?php echo HOMENAME; ?></title>
        <meta  name="keywords"  content="<?php echo $res[0]['c_title']; ?>">
        <meta  name="description"  content="<?php  echo str_replace(" ", "", substr(strip_tags($res[0]['content']), 0, 180)); ?>">
        <?php $this->load->view('sm_commHeader'); ?>
    <div class="daohang"><a href="<?php echo base_url(); ?>" title="<?php echo HOMENAME; ?>">首页</a> > <a href="<?php echo site_url('sm/category/' . ($res[0]['c_id2'])); ?>" title="<?php echo $res[0]['c_title']; ?>"><?php echo $res[0]['c_title']; ?></a> > <?php echo $res[0]['title']; ?></div>
    <div  id="main">
        <div  id="mainleft">
            <div  class="left-post">
                <div  class="post-title">
                    <h1><?php echo $res[0]['title']; ?><a  name="v"></a></h1>
                </div>
                <div  class="post-content">
                    <div  id="imgs">
                        <a  class="prev" href="<?php echo site_url('sm/content/' . ($prev[0]['id'])) ?>"  title="上一张"></a>
                        <img width="396"  style="display:block; float:left;"  src="<?php echo base_url() . "up/" . $res[0]['thumb']; ?>"  alt="<?php echo $res[0]['title']; ?>"  data-bd-imgshare-binded="1"> 
                        <a class="next"  href="<?php echo site_url('sm/content/' . ($next[0]['id'])); ?>"  title="下一张"></a>

                    </div>
                    <div  class="content"> </div>

                </div>
                <div  class="post-footer">
                    <?php echo CONTENTERGG; ?>
                    <p class="pinfo"><a href="javascript:​;">“←键”上一张</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:​;">“→键”下一张</a>&nbsp;&nbsp;&nbsp;&nbsp;<!--<a href="javascript:​;" title="开发中">点击发送弹幕</a>--></p><?php echo $res[0]['content'];echo '本文固定链接：'.site_url('sm/content/' . ($res[0]['id']));  ?>
                    <div  class="clear"></div>

                    <div  id="wumi-related">
                        <div  class="wumii-hook"></div>
                    </div>
                </div>


            </div>
        </div>

        <?php $this->load->view('sm_footer'); ?>