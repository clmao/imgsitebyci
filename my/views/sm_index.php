<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php if (!empty($flag)) { echo $table[0]['c_title'].'_'; }?><?php echo HOMENAME; ?></title>
        <meta  name="keywords"  content="<?php if (!empty($flag)) { echo $table[0]['c_title']; }else{echo HOMEKEY;}?>">
        <meta  name="description"  content="<?php if (!empty($flag)) { echo $table[0]['c_title']; }else{echo HOMEDEC;}?>>
        <?php $this->load->view('sm_commHeader') ?>
        <?php if (!empty($flag)) { ?>
               <div class="daohang"><a href="<?php echo base_url(); ?>" title="<?php echo HOMENAME; ?>">首页</a> > <a href="<?php echo base_url(); ?>" title="<?php echo $table[0]['c_title']; ?>"><?php echo $table[0]['c_title']; ?>
            </a> </div>
    <?php } ?>
    <div  id="main">
        <div  id="mainleft">
            <ul  id="pic-list">


                <?php foreach ($table as $val) { ?>
                    <li  class="item">
                        <div  class="box">
                            <a  href="<?php echo site_url('sm/content/' . $val['id']) ?>" title="<?php echo $val['title']; ?>">
                                <img  alt="<?php echo $val['title']; ?>" title="<?php echo $val['title']; ?>"  src="<?php echo base_url() . 'bp/' . $val['thumb']; ?>">
                            </a>
                            <h3><a  href="<?php echo site_url('sm/content/' . $val['id']) ?>" title="<?php echo $val['title']; ?>"><?php echo $val['title']; ?></a></h3>
                            <p  class="views">浏览：<?php echo $val['show_num']; ?>次&nbsp;&nbsp;&nbsp;&nbsp;分类：<a href="<?php echo site_url('sm/category/' . $val['c_id']) ?>" title="<?php echo $val['c_title']; ?>"><?php echo $val['c_title']; ?></a></p>

                            <p  class="postgg"><?php echo POSTGG;?></p>
                        </div>
                    </li>
                <?php } ?>



                <div  class="clear"></div>
            </ul>
            <?php echo $this->pagination->create_links(); ?>
        </div>
     
            <?php $this->load->view('sm_footer'); ?>