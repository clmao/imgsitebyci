<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta  charset="utf-8">
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <title></title>
        <link  href="<?php echo base_url() . 'public/admin/' ?>css.css"  rel="stylesheet">
        <script  type="text/javascript"  src="<?php echo base_url() . 'public' ?>/jquery.min.js"></script>
        <style  type="text/css"></style>
    </head>
    <body style="background:#ccffcc;">
        <h1 style="margin-top: 20px; margin-left: 20px;">你当前的位置:分类列表</h1>
        <?php if(!empty($_GET['flag'])){echo '<h3 style="color:red;">删除成功</h3>';}?>
        <table class="gridtable">
            <tr>
                <th>ID</th><th>标题</th><th>包含文章数</th><th>操作</th>
            </tr>
            <?php foreach ($table as $val) { ?>
                <tr>
                    <td><?php echo $val['id']; ?></td>
                    <td><?php echo $val['title']; ?></td>
                    <td><?php
                        echo $this->art->cid_conent($val['id']);
                        ?>
                    </td>
                    <td><a href="<?php echo site_url('sa/delCategory') ?>?id=<?php echo $val['id']; ?>&flag=s" onclick="if(!confirm('确定要删除吗')){ return false;}">删除</a> <a href="<?php echo site_url('sa/editCategory') ?>?id=<?php echo $val['id']; ?>&title=<?php echo $val['title']; ?>">编辑</a></td>
                </tr>
            <?php } ?>
        </table>
    <center><?php echo $this->pagination->create_links(); ?></center>
</body></html>
