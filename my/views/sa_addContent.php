<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>上机练习</title>
        <style type="text/css">
            input, textarea {
                padding: 4px;
                border: solid 1px #E5E5E5;
                outline: 0;
                font: normal 13px/100% Verdana, Tahoma, sans-serif;
                width: 300px;
                background: #FFFFFF;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 8px;
                -moz-box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 8px;
                -webkit-box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 8px;    
            }
            textarea {
                width: 400px;
                max-width: 400px;
                height: 150px;
                line-height: 150%;

            }
            input:hover, textarea:hover, input:focus, textarea:focus { border-color: #C9C9C9; }
            label {
                margin-left: 10px;
                color: #999999;
                display:block;
            }
            .submit input {
                width:auto;
                padding: 9px 15px;
                background: #617798;
                border: 0;
                font-size: 14px;
                color: #FFFFFF;
            }
            body { margin:0; margin-left:20px; background:#ccffcc; }
            #viewfile { border:0; padding:0; margin:0;border:1px solid #CCC; }
            #thumb{ border:0; padding:0; margin:0;border:1px solid #CCC; }
            .div { margin-left:0; width:300px; overflow:hidden; padding:20px 0; }
            .line { position:relative; margin:0 auto; width:300px; text-align:left }
            .line span.span { float:left; padding-top:2px; }
            .file { position:absolute; left:0; width:250px; top:0; height:28px; filter:alpha(opacity=0); opacity:0; cursor: pointer }
            .file1 { float:left; margin-left:8px; z-index:1; width:66px; height:28px; line-height:28px; background:url(<?php echo base_url() . 'public/admin/' ?>liulan.gif) no-repeat 0 0; text-indent:-9999px; cursor: pointer }
            .inputstyle { border:1px solid #BEBEBE; width:170px; float:left; height:23px; line-height:23px; background:#FFF; z-index:99 }
            #n{margin-left:10px; width:300px; border:1px solid #CCC;font-size:14px; line-height:30px;}#n a{ padding:0 4px; color:#333}
            select{   
                border-right: #ccc 1px solid;   
                border-top: #ffffff 1px solid;   
                width:200px;
                height:25px;
                text-align:center;
                font-size: 12px;  
                border-left: #ccc 1px solid;   
                color:#999999;   
                border: #ccc 1px solid;   
                background-color: #f4f4f4;   
            }  

        </style>
       
    </head>

    <body>
        <h1>你当前位置：添加内容</h1>
        <?php
        if (!empty($_GET['flag'])) {
            echo "<p style='color:red'>添加成功，请继续添加</p>";
        }
        ?>
        <form enctype="multipart/form-data" action="<?php echo site_url('sa/addContentProcess') ?>" method="post">
            <p class="name">
                <label for="title">标题<?php echo form_error('title', '&nbsp;&nbsp;', '&nbsp;&nbsp;') ?></label>
                <input type="text" name="title" id="name" required="required" value="<?php echo set_value('title') ?>" />

            </p>
            <p class="web">
                <label for="show_num">浏览量<?php echo form_error('show_num', '&nbsp;&nbsp;', '&nbsp;&nbsp;') ?></label>    
                <input type="text" name="show_num" id="name" value="0" required="required" value="<?php echo set_value('show_num') ?>" />
            </p>
            <p class="web">
                <label for="c_id">所属分类</label>    
                <select name="c_id">
                    <?php foreach($c_id as $v){ ?>
                    <option value="<?php echo $v['id'];?>"><?php echo $v['title'];?></option>
                    <?php } ?>
                   
                </select>
            </p>
            
            <div class="div">
                <label for="name">上传图片</label>
                <div class="line">
                    <span class="span">
                        <input  type="text" id="viewfile"  onmouseout="document.getElementById('upload').style.display = 'none';" class="inputstyle" />
                    </span>
                    <label for="unload" onmouseover="document.getElementById('upload').style.display = 'block';" class="file1">浏览...</label>
                    <input type="file" name="viewfile" onchange="document.getElementById('viewfile').value = this.value;
                            this.style.display = 'none';" class="file" id="upload" required="required" />
                </div>
            </div>

            <p class="web">
                <label for="web">是否显示</label>    
                <select name="is_visable">
                    <option value="1">是</option>
                    <option value="0">否</option>
                </select>
            </p>
            <p class="text">
                <label for="text">文字内容<?php echo form_error('content', '&nbsp;&nbsp;', '&nbsp;&nbsp;') ?></label>
                <?php echo $fck;?><?php echo set_value('content') ?>
                
            </p>
            
            <p class="submit">
                <input type="submit" value="提交" />
            </p>
        </form>
    </body>
</html>

