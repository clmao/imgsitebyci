<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta  charset="utf-8">
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <title></title>
        <link  href="<?php echo base_url() . 'public/admin/' ?>css.css"  rel="stylesheet">
        <script  type="text/javascript"  src="<?php echo base_url() . 'public' ?>/jquery.min.js"></script>
        <style  type="text/css"></style>
    </head>
    <body style="background:#ccffcc;">
        <h1 style="margin-top: 20px; margin-left: 20px;">你当前的位置：内容列表</h1>
        <?php if(!empty($_GET['flag'])){echo '<h3 style="color:red;">删除成功</h3>';}?>
        <table class="gridtable">
            <tr>
                <th>ID</th><th>标题</th><th>所属分类</th><th>是否可见</th><th>浏览量</th><th>图片</th><th>时间</th><th>操作</th>
            </tr>
            <?php foreach ($table as $val) { ?>
             <tr>
                <td><?php echo $val['id']; ?></td>
                <td><?php echo $val['title']; ?></td>
                <td><?php echo $val['c_title']; ?></td>
                <td><?php echo $val['is_visable']?"是":"否"; ?></td>
                <td><?php echo $val['show_num']; ?></td>
                <td><?php echo $val['thumb']; ?></td>
                <td><?php echo date('Y-m-d H:i:s',$val['time']); ?></td>
                <td><a href="<?php echo site_url('sa/delcontent') ?>?id=<?php echo $val['id']; ?>&flag=s" onclick="if(!confirm('确定要删除吗')){ return false;}">删除</a> 
                    <a href="<?php echo site_url('sa/edithContent') ?>?id=<?php echo $val['id']; ?>&title=<?php echo $val['title']; ?>&c_id=<?php echo $val['c_id2'] ?>&is_visable=<?php echo $val['is_visable']; ?>&show_num=<?php echo $val['show_num'];?>">编辑</a></td>
            </tr>
            <?php } ?>
        </table>
    <center><?php echo $this->pagination->create_links(); ?></center>
    </body></html>
