<?php
if (!defined('BLOG.CLMAO.COM'))
    exit();

//网站名称
define('HOMENAME','SM趣味站');

//首页关键字
define('HOMEKEY','SM趣味站,搞笑');

//首页描述
define('HOMEDEC','SM趣味站为广大屌丝提供搞笑、趣味图片');

//验证码halt规则
define('VERRULE',-10);

//后台密码
define('ADMINPWS','admin');

//列表页图片下面的广告代码,建议宽度为200PX
define('POSTGG','');

//边栏的广告，建议宽度为250PX
define('SIDERGG','');

//版权底部处的的广告，建议高度为30PX
define('FOOTERGG','');

//内容页图片下面的广告，建议宽度为636px
define('CONTENTERGG','');

//ICO路径
define('ICOPATH','/sm/public/favicon.ico');