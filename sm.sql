-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 06 月 15 日 22:19
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `sm`
--

-- --------------------------------------------------------

--
-- 表的结构 `sm_category`
--

CREATE TABLE IF NOT EXISTS `sm_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(10) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `sm_category`
--

INSERT INTO `sm_category` (`id`, `title`) VALUES
(1, '未分类'),
(3, 'NBA趣图'),
(4, '地球怪物');

-- --------------------------------------------------------

--
-- 表的结构 `sm_content`
--

CREATE TABLE IF NOT EXISTS `sm_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '',
  `thumb` char(21) DEFAULT '',
  `big_img` char(21) DEFAULT '',
  `content` text,
  `c_id` tinyint(4) DEFAULT '1',
  `t_id` tinyint(4) DEFAULT '0',
  `is_visable` char(1) DEFAULT '1',
  `show_num` int(10) DEFAULT '0',
  `time` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `sm_content`
--

INSERT INTO `sm_content` (`id`, `title`, `thumb`, `big_img`, `content`, `c_id`, `t_id`, `is_visable`, `show_num`, `time`) VALUES
(24, '逗比斯蒂芬森向詹姆斯吹气', '051/140150675543.jpg', '051/140150675543.jpg', '<p>这场步行者的获胜离不开斯蒂芬森的努力啊</p>\r\n<p>詹姆斯居然只能拿到了7分</p>\r\n<p>只因为他早早5犯了</p>', 3, 0, '1', 305, 1401506755),
(25, '斯蒂芬森捂嘴激怒吾皇', '051/140151172128.jpg', '051/140151172128.jpg', '<p>在G6一站，热火大比分战胜步行者，步行者连续2年止步东部决赛</p>', 3, 0, '1', 108, 1401511721),
(28, '邓肯拥抱妖刀庆祝命中关键三分球', '061/140159550289.jpg', '061/140159550289.jpg', '<p>&nbsp; <span>� �血沸腾的G6！恭喜马刺晋级总决赛！</span><br />\r\n<span>� �大的马努！冷血的三分，鬼魅的传球，暴力的篮板！</span><br />\r\n<span>� �大的石佛！那是青春的记忆！</span><br />\r\n<span>� �霆队也值得我们去尊敬！年轻人，未来属于你们！</span><br />\r\n<span>PS 生命不息，正义不止。</span><br />\r\n<span>� �家的心情可以理解，是国人都应愤怒！</span><br />\r\n<span>� �生活还得继续，评论也得继续。</span><br />\r\n<span>体育版块应多一点理性评球，少一点无脑愤慨。</span></p>', 3, 0, '1', 101, 1401595502),
(36, '2亿年前恐龙虾“复活”', '061/140160170464.jpg', '061/140160170464.jpg', '<p>&nbsp;这种生物我家周围有很多，一直以来就有没什么稀奇的。他们不会咬人不会有任何攻击性，生活在水里，平时都是反过来脚朝上浮在水面上。我小时候总是抓来好多来给小鸡吃。<br />\r\n网易贵州省六盘水市网友(219.141.*.*)的原贴：2<br />\r\n我们当地人叫它&ldquo;王八盖子&rdquo;。<br />\r\n这有什么好稀奇的，水稻田里多的是，尤其是刚插完秧那会。</p>\r\n<p>&nbsp;</p>', 4, 0, '1', 504, 1401601704);

-- --------------------------------------------------------

--
-- 表的结构 `sm_link`
--

CREATE TABLE IF NOT EXISTS `sm_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '',
  `src` varchar(100) DEFAULT '',
  `flag` varchar(50) DEFAULT '''link''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `sm_link`
--

INSERT INTO `sm_link` (`id`, `title`, `src`, `flag`) VALUES
(3, '百度', 'http://www.baidu.com', 'link'),
(5, '返回博客', 'http://blog.clmao.com', 'meun');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
