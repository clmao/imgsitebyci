http://git.oschina.net/clmao/imgsitebyci

=======
## 在前面
本程序采用Ci 2.1.4.开发，是一个图片笑话网站。

## 使用方法：<br/>
首先导入SQL文件sm.sql，<br/>
然后修改文件\my\config\database.php，配置你的数据库信息，<br/>
运行index.php文件。<br/>
后台路径：localhost//index.php/sm/login  默认帐户：admin,密码：admin, 验证码需要 -10 （意思就是你看的验证是1000，那么你应该输入的是990）<br/>
我把后台密码和验证码的输入规则都写在根目录下的config.php文件夹里面了，不知道大家会不会喷我，嘻嘻。<br/>
大一时候练习CI写的网站，后台很丑陋。<br/>

##预览图<br/>
![1](http://git.oschina.net/uploads/images/2014/1009/115558_239a127e_106233.jpeg)

![2](http://git.oschina.net/uploads/images/2014/1009/115558_72dba33e_106233.jpeg)


## 联系方式
有问题请留言我的<a href="http://blog.clmao.com/?page_id=145" target="_blank">博客</a>
